#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [hashtable] $AppManifest,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [hashtable] $DeploymentManifest,

    [Parameter(Mandatory=$true, Position=2)]
    [hashtable] $Config,

    [Parameter(Mandatory=$true, Position=3)]
    [hashtable] $Context,

    [Parameter(Mandatory=$true, Position=4)]
    [hashtable] $Params
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifest -eq $null) {
    $DeploymentManifest = $AppManifest
}

# Reading parameters
$service = $Context.service
if ($service -eq $null) {
    throw "Service name is missing in the action step"
}

$namespace = $Context.namespace
if ($namespace -eq $null) {
    throw "Namespace is missing in the action step"
}

$component = $Context.component
if ($component -eq $null) {
    throw "Component is missing in the action step"
}
$component = Get-AppComponent -Manifest $AppManifest -Component $Context.component
if($component.registry -ne $null){
    $component.registry = "$($component.registry)/"
} if($component.version -ne $null){
    $component.version = ":$($component.version)"
}
$image = "$($component.registry)$($component.name)$($component.version)"

$imagePullPolicy = $Context.image_pull_policy
if ($imagePullPolicy -eq $null) {
    $imagePullPolicy = "Always" #IfNotPresent
}

# Set deployment replicas
$deploymentReplicas = ""
if ($Context.replicas -ne $null) {
    $deploymentReplicas = "  replicas: $($Context.replicas)"
} elseif ($Context.hpa -ne $null) {
    $deploymentReplicas = "  replicas: $($Context.hpa.min_replicas)"
} else {
    $deploymentReplicas = "  replicas: 1"
}

# k8s deployment
$envVars = "        env: `n"
$envVarCount = 0
if($Context.env_variables -ne $null){
    foreach ($envVarName in $($Context.env_variables.Keys)) {
        if($envVarCount -gt 0){
            $envVars += "`n"
        }
        
        if ($envVarName -eq "env_field_refs") {
            # process fieldRef variables
            foreach ($linkedEnvVarName in $($Context.env_variables["env_field_refs"].Keys)) {
                $envVars += "        - name: $linkedEnvVarName `n"
                $envVars += "          valueFrom: $($Context.env_variables["env_field_refs"][$linkedEnvVarName]) "
                $envVarCount++;
            }
        } else {
            # regular env var
            $envVars += "        - name: $envVarName `n"
            $envVars += "          value: `"$($Context.env_variables[$envVarName])`" "
            $envVarCount++;
        }
    }
}

if($Context.env_mappings -ne $null){
    foreach ($envMappings in $Context.env_mappings) {
        $configName = $($envMappings["name"])
        $objType = $($envMappings["object_type"])
        foreach ($envVarName in $($envMappings["mappings"].Keys)) {
            if($envVarCount -gt 0){
                $envVars += "`n"
            }
            if($objType -ne "secret") {
                $objType = "configMap"
            }
            # Note - there are 2 more spaces here than in docker-service!
            $envVars += "        - name: $envVarName `n"
            $envVars += "          valueFrom: `n"
            $envVars += "            $($objType)KeyRef: `n"
            $envVars += "              name: $configName `n"
            $envVars += "              key: $($envMappings["mappings"][$envVarName]) "

            $envVarCount++;
        }
    }
}

# Set custom volumes, if required
if ($Context.volumes -ne $null -and $Context.volumes.enabled -ne $false) {
    $volumeMounts = "        volumeMounts:`n"
    $volumes = "      volumes:`n"

    foreach ($volume in $Context.volumes) {
        $volumeMounts += "        - name: $($volume.name)`n"
        $volumeMounts += "          mountPath: $($volume.mount_path)`n"

        $volumes += "      - name: $($volume.name)`n"
        if ($volume.data.type -eq "emptyDir") {
            $volumes += "        emptyDir: {}`n"
        } elseif ($volume.data.type -eq "configMap") {
            $volumes += "        configMap:`n"
            $volumes += "          name: $($volume.data.name)`n"
        } elseif ($volume.data.type -eq "secret") {
            $volumes += "        secret:`n"
            $volumes += "          secretName: $($volume.data.name)`n"
        }
    }
}

$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Single port service
#   The next 5 fields are optional, and allow setting up a single port 
#   service. If neither “nodeport” or “targetport” are set, cluster-ip 
#   will be used with a service port of 8080.
#       "nodeport": "<port to expose on the node>", // optional
#       "port": "<service’s port>", // optional, default: 8080
#       "targetport": "<container’s port to target>", // optional, default:8080
#       "containerport": "<container’s port>", // optional - defaults to: 
#                                                nodeport/targetport/8080
#       "protocol": "<port’s protocol>", // optional, default: TCP
$containerport = $Context.containerport
$nodeport = $Context.nodeport
$targetport = $Context.targetport
$svcport = $Context.port; if($svcport -eq $null) { $svcport = "8080" }
$protocol = $Context.protocol; if($protocol -eq $null) { $protocol = "TCP" }
if ($nodeport -ne $null) {
    $inputPath = "$rootPath/templates/k8s/service-node-port.yml"
    if($containerport -eq $null){
        if($targetport -ne $null){
            $containerport = $targetport
        } else {
            $containerport = $nodeport
        }
    }

    # Set k8s service port
    $k8sServicePorts =  " - port: $svcport`n"
    $k8sServicePorts += "   nodePort: $nodeport`n"
    if($targetport -ne $null) { $k8sServicePorts += "   targetPort: $targetport`n" }
    $k8sServicePorts += "   protocol: $protocol"

    # Set k8s deployment port
    $k8sDeploymentPorts = "        - containerPort: $containerport"
} elseif ($targetport -ne $null) {
    $inputPath = "$rootPath/templates/k8s/service-cluster-ip.yml"
    if($containerport -eq $null){
        $containerport = $targetport
    }

    # Set k8s service port
    $k8sServicePorts =  " - port: $svcport`n"
    $k8sServicePorts += "   targetPort: $targetport`n"
    $k8sServicePorts += "   protocol: $protocol"

    # Set k8s deployment port
    $k8sDeploymentPorts = "        - containerPort: $containerport"
} else {
    $inputPath = "$rootPath/templates/k8s/service-cluster-ip.yml"
    if($containerport -eq $null){
        $containerport = "8080"
    }

    # Set k8s service port
    $k8sServicePorts =  " - port: $svcport`n"
    $k8sServicePorts += "   protocol: $protocol"

    # Set k8s deployment port
    $k8sDeploymentPorts = "        - containerPort: $containerport"
}

# Multiport service
if ($Context.multiport_service -ne $null) {
    switch ($Context.multiport_service.type) {
        "ClusterIp" { 
            $inputPath = "$rootPath/templates/k8s/service-cluster-ip.yml" 
            
            # Set k8s service ports
            $k8sServicePorts = ""
            foreach ($svcPort in $Context.multiport_service.ports) {
                if($svcPort.protocol -eq $null){ $svcPort.protocol = "TCP" }
                $k8sServicePorts += " - name: $($svcPort.name)`n"
                $k8sServicePorts += "   port: $($svcPort.port)`n"
                $k8sServicePorts += "   targetPort: $($svcPort.target_port)`n"
                $k8sServicePorts += "   protocol: $($svcPort.protocol)`n"
            }
        }
        "NodePort" { 
            $inputPath = "$rootPath/templates/k8s/service-node-port.yml" 

            # Set k8s service ports
            $k8sServicePorts = ""
            foreach ($svcPort in $Context.multiport_service.ports) {
                if($svcPort.protocol -eq $null){ $svcPort.protocol = "TCP" }
                $k8sServicePorts += " - name: $($svcPort.name)`n"
                $k8sServicePorts += "   port: $($svcPort.port)`n"
                $k8sServicePorts += "   targetPort: $($svcPort.target_port)`n"
                $k8sServicePorts += "   nodePort: $($svcPort.node_port)`n"
                $k8sServicePorts += "   protocol: $($svcPort.protocol)`n"
            }
        }
    }

    # Set k8s deployment ports
    $k8sDeploymentPorts = ""
    foreach ($svcPort in $Context.multiport_service.ports) {
        $k8sDeploymentPorts += "        - name: $($svcPort.name)`n"
        $k8sDeploymentPorts += "          containerPort: $($svcPort.target_port)`n"
    }
    
}

# Set readiness probe
$readinessProbe = ""
if ($Context.readiness_probe -ne $null) {
    $readinessProbe += "        readinessProbe:`n"
    if ($Context.readiness_probe.tcp_socket_port -ne $null) {
        $readinessProbe += "          tcpSocket:`n"
        $readinessProbe += "            port: $($Context.readiness_probe.tcp_socket_port)`n"
    } elseif ($Context.readiness_probe.http_get -ne $null) { #httpGet
        $readinessProbe += "          httpGet:`n"
        $readinessProbe += "            path: $($Context.readiness_probe.http_get.path)`n"
        $readinessProbe += "            port: $($Context.readiness_probe.http_get.port)`n"
    } else { # exec command by default
        $readinessProbe += "          exec:`n"
        $readinessProbe += "            command: $($Context.readiness_probe.command)`n"
    }
    $readinessProbe += "          initialDelaySeconds: $($Context.readiness_probe.initial_delay_seconds)`n"
    $readinessProbe += "          periodSeconds: $($Context.readiness_probe.period_seconds)"
}

# Set liveness probe
$livenessProbe = ""
if ($Context.liveness_probe -ne $null) {
    $livenessProbe += "        livenessProbe:`n"
    if ($Context.liveness_probe.tcp_socket_port -ne $null) {
        $livenessProbe += "          tcpSocket:`n"
        $livenessProbe += "            port: $($Context.liveness_probe.tcp_socket_port)`n"
    } elseif ($Context.liveness_probe.http_get -ne $null) { #httpGet
        $livenessProbe += "          httpGet:`n"
        $livenessProbe += "            path: $($Context.liveness_probe.http_get.path)`n"
        $livenessProbe += "            port: $($Context.liveness_probe.http_get.port)`n"
    } else { # exec command by default
        $livenessProbe += "          exec:`n"
        $livenessProbe += "            command: $($Context.liveness_probe.command)`n"
    }
    $livenessProbe += "          initialDelaySeconds: $($Context.liveness_probe.initial_delay_seconds)`n"
    $livenessProbe += "          periodSeconds: $($Context.liveness_probe.period_seconds)"
}

# Set deployment resources
$resources = ""
if ($Context.resources -ne $null) {
    $resources += "        resources:`n"
    if ($Context.resources.requests -ne $null) {
        $resources += "          requests:`n"
        $resources += "            cpu:  $($Context.resources.requests.cpu)`n"
        $resources += "            memory: $($Context.resources.requests.memory)`n"
    }
    if ($Context.resources.limits -ne $null) {
        $resources += "          limits:`n"
        $resources += "            memory: $($Context.resources.limits.memory)`n"
    }
}

$tempPath = $Params.TempPath
$outputPath = "$tempPath/service-$service.yml"

# Set k8s deployment api version
$deploymentApiVersion = "apps/v1"
if ($Context.deployment_api_version -ne $null) {
    $deploymentApiVersion = $Context.deployment_api_version
}

    # Set deployment annotations
$deploymentAnnotations = ""
if ($Context.deployment_annotations -ne $null) {
    $deploymentAnnotations = "  annotations:`n"
    foreach ($deploymentAnnotation in $Context.deployment_annotations) {
        $deploymentAnnotations += "    $($deploymentAnnotation.name): `"$($deploymentAnnotation.value)`"`n"
    }
}

# Set additional containers
$additionalContainers = ""
if ($Context.additional_containers -ne $null) {
    
    foreach ($additionalContainer in $Context.additional_containers) {
        $additionalContainers += "      - name: $($additionalContainer.name)`n"
        $additionalContainers += "        image: $($additionalContainer.image)`n"
        
        if ($additionalContainer.resources -ne $null) {
            $additionalContainers += "        resources:`n"
            if ($additionalContainer.resources.requests -ne $null) {
                $additionalContainers += "          requests:`n"
                $additionalContainers += "            cpu: $($additionalContainer.resources.requests.cpu)`n"
                $additionalContainers += "            memory: $($additionalContainer.resources.requests.memory)`n"
            }
            if ($additionalContainer.resources.limits -ne $null) {
                $additionalContainers += "          limits:`n"
                $additionalContainers += "            memory: $($additionalContainer.resources.limits.memory)`n"
            }
        }

        if ($additionalContainer.args -ne $null) {
            $additionalContainers += "        args:`n"
            foreach ($additionalContainerArg in $additionalContainer.args) {
                $additionalContainers += "        - $additionalContainerArg`n"
            }
        }

        if ($additionalContainer.volumes -ne $null) {
            $additionalContainers += "        volumeMounts:`n"
            foreach ($additionalContainerVolume in $additionalContainer.volumes) {
                $additionalContainers += "          - name: $($additionalContainerVolume.name)`n"
                $additionalContainers += "            mountPath: $($additionalContainerVolume.mount_path)`n"
            }
        }
    }
}

# Enable/Disable imagePullSecret
$dockerAuthEnabled = ConvertTo-EnvBoolean $Context.docker_auth
if($dockerAuthEnabled) {
    $imagePullSecrets =  "      imagePullSecrets: `n"
    $imagePullSecrets += "        - name: auth "
}

$templateParams = @{ 
    api_version = $deploymentApiVersion
    service = $service
    namespace = $namespace
    nodeport = $nodeport
    targetport = $targetport
    image = $image
    image_pull_policy = $imagePullPolicy
    deployment_replicas = $deploymentReplicas
    volume_mounts = $volumeMounts
    env_vars = $envVars
    volumes = $volumes
    service_ports = $k8sServicePorts
    deployment_ports = $k8sDeploymentPorts
    readiness_probe = $readinessProbe
    liveness_probe = $livenessProbe
    resources = $resources
    deployment_annotations = $deploymentAnnotations
    additional_containers = $additionalContainers
    image_pull_secrets = $imagePullSecrets
}

Build-EnvTemplate -InputPath $inputPath -OutputPath $outputPath -Params1 $templateParams

# Create hpa if required
if ($Context.hpa -ne $null) {
    $templateParams = @{ 
        service = $service
        namespace = $namespace
        min_replicas = $Context.hpa.min_replicas
        max_replicas = $Context.hpa.max_replicas
        target_cpu = $Context.hpa.target_cpu
    }

    $hpaOutPath = "$tempPath/hpa-$service.yml"
    Build-EnvTemplate -InputPath "$rootPath/templates/k8s/hpa.yml" -OutputPath $hpaOutPath -Params1 $templateParams

    kubectl apply -f $hpaOutPath
    Remove-Item -Path $hpaOutPath
}

# Create vpa if required
if ($Context.vpa -ne $null -and $Context.vpa.enabled) {
    $templateParams = @{ 
        name = $service
        namespace = $namespace
    }

    $vpaOutPath = "$tempPath/vpa-$service.yml"
    Build-EnvTemplate -InputPath "$rootPath/templates/k8s/vpa.yml" -OutputPath $vpaOutPath -Params1 $templateParams

    kubectl apply -f $vpaOutPath
    Remove-Item -Path $vpaOutPath
}

kubectl apply -f $outputPath

Remove-Item -Path $outputPath