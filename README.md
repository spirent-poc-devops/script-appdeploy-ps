# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> Spirent VisionWorks Application Deployment Scripts

This is a set of scripts to automate deployment of Spirent applications.

<a name="links"></a> Quick links:
* [Change Log](CHANGELOG.md)

## Use

Start the process from any computer. Download released package with application deployment scripts from [link...](http://somewhere.com/script-appdeploy-ps-1.0.0.zip)
```bash
wget http://somewhere.com/script-appdeploy-ps-1.0.0.zip
```

Unzip scripts from the downloaded archive
```bash
unzip script-appdeploy-ps-1.0.0.zip
```

Prior to installing applications, create an environment using environment provisioning scripts.
Environment configuration and resource files are required for the application deployment, so keep them in a location accessible by the scripts.
If the target environment was created from a different machine, copy the environment configuration and resource files over.

Download release packages for the applications you are planning to deploy. For example, Sample Application can downloaded from [here...](http://somewhere.com/package-sampleapp-1.0.0.zip)
```bash
wget http://somewhere.com/package-sampleapp-1.0.0.zip
```

Unzip application package from the downloaded archive
```bash
unzip package-sampleapp-1.0.0.zip
```

### Caching components
If application shall be installed at a remote location without good or any Internet connection, before going to the installation site, you need to cache the application artifacts. To do this, first install the prerequisites using:
```bash
install_prereq_ubuntu.sh
```
And then cache the components using:
```bash
cache_components.ps1 -Manifest <path to application manifest> -Config <path to env config file>
```

These can then be imported to the nodes of the onprem environment by running:
```bash
import_components.ps1 -Manifest <path to application manifest> -Config <path to env config file>
```

### Installing
To install the application, use:
```bash
install_app.ps1 -Manifest <path to application manifest> [-Deployment <path to deployment manifest>] -Config <path to env config file> [-Resource <path to env resource file>] [-Partition <partition name>]
```

If the application was previously installed, you can upgrade it without loosing data or interrupting user requests.
```bash
upgrade_app.ps1 -Manifest <path to application manifest> [-Deployment <path to deployment manifest>] -Config <path to env config file> [-Resource <path to env resource file>] [-Partition <partition name>]
```

Uninstall the application when its no longer needed to free up used computing resources.
```bash
uninstall_app.ps1 -Manifest <path to application manifest> [-Deployment <path to deployment manifest>] -Config <path to env config file> [-Resource <path to env resource file>] [-Partition <partition name>]
```

## Application manifests
Application manifest contains common infromation about the application and it's components:

See the example sampleapp application manifest below and its comments 
```json
{
  // Installation's name
  "name": "sampleapp",
  // Installation's version
  "version": "1.0.0",
  // Installation's description
  "description": "Spirent Sample Application",
  // Installation's author(s)
  "author": "Sergey S.",
  // Components used in the installation
  "components": [
    {
      "name": "sampleapp-node",
      "version": "1.0.0",
      "type": "container",
      "registry": "ghcr.io/testrepo"
      // Note: Component's registry + name + version are used to pull the image, 
      // so make sure they match what is set in the registry and are the same format.
    },
    // ...
    {
      "name": "samplelambda-node",
      "version": "1.0.0",
      "type": "npm",
      "scope": "@spirent"
    }
  ]
}
```

## Deployment manifests
Deployment manifest can contain the following standard actions:
- namespace - creates a namespace for the application;
- docker-auth - used for creating a secret with registry credentails, so that private application images can be pulled;
- docker-component;
- docker-service;
- http-request - initiates an http request;
- wait - waits the indicated time;

Deployment manifest is not mandatory, in case when deployment manifest is missing all actions described in application manifest.

See the example sampleapp deployment manifest below and its comments 
```json
{
  // Installation's name
  "name": "sampleapp",
  // Installation's version
  "version": "1.0.0",
  // What the installation depends on
  "dependencies": [
    { "name": "appDep1", "version": ">=1.0.0" },
    { "name": "appDep2", "version": ">=2.1.0" }
  ],
  // Block with installation instructions
  "install-upgrade-uninstall": [
    {
      // Which environments the following steps are supported on
      "environments": [
          { "name": "dev", "version": ">=1.0.0" },
          { "name": "onprem", "version": ">=1.0.0" },
          { "name": "test", "version": ">=1.0.0" },
          { "name": "cloudaws", "version": ">=1.0.0" }
      ],
      // Installation steps, in the order they should be run in
      "steps": [
        {
          // Standard action for creating a namespace for the application
          "action": "namespace",
          "namespace": "sampleapp"
        },
        {
          // Standard action for copying a config to the application's namespace
          // Note: in this step, we're copying a configmap
          "action": "copy-object",
          "from_namespace" : "infra",
          "to_namespace" : "sampleapp",
          "object_type" : "configmap",
          "name" : "connection-params"
        },
        {
          // Standard action for copying a config to the application's namespace
          // Note: in this step, we're copying a secret
          "action": "copy-object",
          "from_namespace" : "infra",
          "to_namespace" : "sampleapp",
          "object_type" : "secret",
          "name" : "connection-creds"
        },
        {
          // Standard action for creating a secret with registry credentails, 
          // so that private application images can be pulled
          "action": "docker-auth",
          "namespace": "sampleapp"
        },
        {
          // Standard action for applying a k8s script for the application
          "action": "k8s-script",
          // Note: path to script is relative to the application's manifest
          "script": "k8s/config-test.yml"
        },
        // ...
        {
          // Standard action for deploying a service
          "action": "docker-service",
          // Service's name
          "service": "sampleapp",
          // Namespace to deploy the service in
          "namespace": "sampleapp",
          // Component name 
          // Note: must match the component name in the "components" section of the manifest
          "component": "sampleapp-node",
          "targetport": "8080",
          // Desired replicas
          "replicas": "2",
          // Environment variable to set
          "env_variables": { 
            "POSTGRES_ENABLED": "true",
            "HTTP_ENABLED": "true",
            "HTTP_PORT": "8080" 
          }
        },
        {
          "action": "delay",
          "execute-on": ["install"],
          "seconds": "30"
        },
        {
          // Standard action for making http requests
          "action": "http-request",
          // Only run this step when installing the application
          "execute-on": ["install"],
          "verb": "POST",
          // Note: <%=env.url%> is a templating tag - it will automatically be populated with
          // a value from the environment's config and/or resources file
          "url": "http://<%=env.url%>/sampleapp",
          "content_type": "application/json",
          // Data to include in the POST request
          "data_file": "data/seedingdata.json"
        },
        {
          "action": "http-request",
          "execute-on": ["uninstall"],
          "verb": "DELETE",
          "url": "http://<%=env.url%>/sampleapp"
        }
      ]
    }
  ]
}
```

## Baselines

#### Overview

Baseline is an abstract concept of components versions which make system work. For example we have running environment with many components, at some point we tested it and agreed that the system works properly, so we want to make a baseline of this system. To do that we need to compare components running in environment with components listed in manifest file. This is done by `baseline_app.ps1` script. When you execute this script, it will:

* Verify that each of the components from manifest file deployed to your environment.
* Manifest component version and running component version are equals, otherwise it will update the version in manifest.json file
* Verifies that all running components are present in manifest file.

#### Use

```bash
.\baseline_app.ps1 -ManifestPath <path to manifest json file> -ConfigPath <path to env config file> [-Resource <path to env resource file>] -Version <system version> -Namespace <namespace of system components>
```

#### Example

```bash
> .\baseline_app.ps1 -ManifestPath .\temp\manifest-v2.0.1.json -ConfigPath ..\env-dev-ps\config\config.json -Version 2.0.1 -Namespace sampleapp
```

```
Processing component service-sample-java...
Processing component service-sample2-java...
Processing component service-sample3-java...
Updating component version in the manifest file...
```

## Contacts

This environment was created and currently maintained by the team managed by *Brandon Wilburn* .
