#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [hashtable] $AppManifest,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [hashtable] $DeploymentManifest,

    [Parameter(Mandatory=$true, Position=2)]
    [hashtable] $Config,

    [Parameter(Mandatory=$true, Position=3)]
    [hashtable] $Context,

    [Parameter(Mandatory=$true, Position=4)]
    [hashtable] $Params
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifest -eq $null) {
    $DeploymentManifest = $AppManifest
}

# Reading parameters
$component = $Context.component
if ($component -eq $null) {
    throw "Component is missing in the action step"
}

$name = $Context.name
if ($name -eq $null) {
    $name = $Context.component
}

$component = Get-AppComponent -Manifest $AppManifest -Component $Context.component
if($component.registry -ne $null){
    $component.registry = "$($component.registry)/"
} if($component.version -ne $null){
    $component.version = ":$($component.version)"
}
$image = "$($component.registry)$($component.name)$($component.version)"

$imagePullPolicy = $Context.image_pull_policy
if ($imagePullPolicy -eq $null) {
    $imagePullPolicy = "Always" #IfNotPresent
}

$env = $Context.env

if ($Context.command -ne $null) {
    $command = "            command: `n"
    foreach ($line in $Context.command){
        $command += "            - $line `n"
    }
    $command = $command.Substring(0,$command.Length-1)
}

$schedule = $Context.schedule
if ($schedule -eq $null) {
    throw "Schedule (e.g. `"0 8 * * *`") is missing in the action step"
}
$schedule = "`'" + $schedule + "`'"

# k8s deployment
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
$inputPath = "$rootPath/templates/k8s/cronjob.yml"

$tempPath = $Params.TempPath
$outputPath = "$tempPath/cronjob-$name.yml"

$templateParams = @{ 
    name = $name
    image = $image
    imagePullPolicy = $imagePullPolicy
    command = $command
    env = $env
    schedule = $schedule
}

Build-EnvTemplate -InputPath $inputPath -OutputPath $outputPath -Params1 $templateParams

kubectl apply -f $outputPath

Remove-Item -Path $outputPath