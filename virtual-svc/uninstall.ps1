#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [hashtable] $AppManifest,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [hashtable] $DeploymentManifest,

    [Parameter(Mandatory=$true, Position=2)]
    [hashtable] $Config,

    [Parameter(Mandatory=$true, Position=3)]
    [hashtable] $Context,

    [Parameter(Mandatory=$true, Position=4)]
    [hashtable] $Params
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifest -eq $null) {
    $DeploymentManifest = $AppManifest
}

# Reading parameters
$name = $Context.name
if ($name -eq $null) {
    throw "Name is missing in the action step"
}

$namespace = $Context.namespace
if ($namespace -eq $null) {
    throw "Namespace is missing in the action step"
}

$svcGateway = $Context.gateway
if ($svcGateway -eq $null) {
    throw "Gateway is missing in the action step"
}

$svcHost = $Context.host
if ($svcHost -eq $null) {
    throw "Host is missing in the action step"
}

$matches = $Context.matches
if ($matches -eq $null) {
    throw "Matches list is missing in the action step"
}

$httpMatches = "  - match: `n"
foreach ($match in $matches) {
    if(($match.prefix -eq $null) -or ($match.url -eq $null) -or ($match.port -eq $null)) {
        throw "An element in the list of matches is missing prefix, url, or port";
    }
    $httpMatches += "    - uri: `n"
    $httpMatches += "        prefix: $($match.prefix) `n"
    $httpMatches += "    route: `n"
    $httpMatches += "    - destination: `n"
    $httpMatches += "        host: $($match.url) `n"
    $httpMatches += "        port: `n"
    $httpMatches += "          number: $($match.port) `n"   
}

$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
$inputPath = "$rootPath/templates/virtual-svc.yml"

$tempPath = $Params.TempPath
$outputPath = "$tempPath/virt-svc.yml"

$templateParams = @{ 
    name = $name
    namespace = $namespace
    gateway = $svcGateway
    host = $svcHost
    matches = $httpMatches
}

Build-EnvTemplate -InputPath $inputPath -OutputPath $outputPath -Params1 $templateParams

kubectl delete -f $outputPath

Remove-Item -Path $outputPath