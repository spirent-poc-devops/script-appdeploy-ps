$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

function Read-EnvApplications
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$false, Position=0)]
        [string] $Namespace="infra",
        [Parameter(Mandatory=$false, Position=1)]
        [string] $ConfigMap="applications"
    )

    # Reading connection parameters
    try {
        $temp = (kubectl get configmaps $ConfigMap -n $Namespace -o json) | Out-String | ConvertFrom-Json
        $applications = ConvertTo-EnvHashtable $temp
        if ($applications -eq $null) { $applications = @{} }
        $applications = $applications["data"]
        if ($applications -eq $null) { $applications = @{} }
        return $applications
    } catch {
        return @{}
    }
}

function Write-EnvApplications
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, Position=0)]
        [hashtable] $Applications,
        [Parameter(Mandatory=$false, Position=1)]
        [string] $Namespace="infra",
        [Parameter(Mandatory=$false, Position=2)]
        [string] $ConfigMap="applications"
    )

    kubectl delete --ignore-not-found=true configmap $ConfigMap -n $Namespace | Out-Null
    $command = "kubectl create configmap $ConfigMap -n $Namespace"
    foreach ($item in $applications.GetEnumerator()) {
        $command = $command + " --from-literal=" + $item.Name + "=" + $item.Value
    }

    Invoke-Expression -Command $command | Out-Null

    return $Applications
}

function Read-EnvEnvironment
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$false, Position=0)]
        [string] $Namespace="infra",
        [Parameter(Mandatory=$false, Position=1)]
        [string] $ConfigMap="environment"
    )

    # Reading environment settings
    try {
        $temp = (kubectl get configmaps $ConfigMap -n $Namespace -o json) | Out-String | ConvertFrom-Json
        $environment = ConvertTo-EnvHashtable $temp
        if ($environment -eq $null) { $environment = @{} }
        $environment = $environment["data"]
        if ($environment -eq $null) { $environment = @{} }
        return $environment
    } catch {
        return @{}
    }
}