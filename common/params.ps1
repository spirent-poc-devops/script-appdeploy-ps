$path = $PSScriptRoot
if ($path -eq "") { $path = "." }

function Read-EnvParams
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$false, Position=0)]
        [string] $Namespace="infra",
        [Parameter(Mandatory=$false, Position=1)]
        [string] $ConfigMap="connection-params",
        [Parameter(Mandatory=$false, Position=2)]
        [string] $Secret="connection-creds"
    )

    # Reading connection parameters
    $temp = (kubectl get configmaps $ConfigMap -n $Namespace -o json) | Out-String | ConvertFrom-Json
    $connectionParams = ConvertTo-EnvHashtable $temp
    $connectionParams = $connectionParams["data"]

    # Reading connection credentials
    $temp = (kubectl get secrets $Secret -n $Namespace -o json) | Out-String | ConvertFrom-Json
    $connectionCreds = ConvertTo-EnvHashtable $temp
    $connectionCreds = $connectionCreds["data"]

    # Decoding connection credentials and merging with parameters
    $params = $connectionParams
    foreach ($item in $connectionCreds.GetEnumerator()) {
        $params[$item.Name] = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($item.Value))
    }

    return $connectionParams
}
