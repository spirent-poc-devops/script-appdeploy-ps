#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $AppManifestPath,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [string] $DeploymentManifestPath,

    [Parameter(Mandatory=$false, Position=2)]
    [string] $Task
)

# Stop on error
$ErrorActionPreference = "Stop"

# Load common functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Set flag to use a single manifest if a Deployment manifest is not set
$SingleManifestMode = $DeploymentManifestPath -eq ""

# Set default parameter values
$packagePath = (Get-Item $AppManifestPath).Directory.FullName

# Read connection parameters
$config = Read-EnvParams

# Check for and fill in templated vars in appManifest, then read the appManifest
$appManifestTemplatePath = $AppManifestPath
$AppManifestPath = "$rootPath/../temp/app_manifest.json"
Build-EnvTemplate -InputPath $appManifestTemplatePath -OutputPath $AppManifestPath -Params1 $config
$appManifest = Read-AppManifest -ManifestPath $AppManifestPath

if(!$SingleManifestMode){
    # Check for and fill in templated vars in deploymentManifest, then read the deploymentManifest
    $deploymentManifestTemplatePath = $DeploymentManifestPath
    $DeploymentManifestPath = "$rootPath/../temp/deployment_manifest.json"
    Build-EnvTemplate -InputPath $deploymentManifestTemplatePath -OutputPath $DeploymentManifestPath -Params1 $config
    $deploymentManifest = Read-AppManifest -ManifestPath $DeploymentManifestPath
} else {
    # Just use the same manifest we created above
    $deploymentManifest = $appManifest
}

# Read applications array from resources file
$applications = Read-EnvApplications

Write-Host "***** Performing $Task for application $($appManifest.name):$($appManifest.version) *****"

# Checking dependencies
$dependencies = $deploymentManifest.dependencies
if (($task -ne "uninstall") -and ($dependencies -ne $null)) {
    Write-Host "`n***** Checking dependencies... *****`n"

    foreach ($dependency in $dependencies) {
        $installedVersion = $applications[$dependency.name]
        if ($installedVersion -eq $null) {
            Write-Error "Dependency $($dependency.name):$($dependency.version) - Missing"
        } else {
            # Check version
            if (Test-AppVersion -Version $installedVersion -Pattern $Dependency.version) {
                Write-Host "Dependency $($dependency.name):$($dependency.version) - OK"
            } else {
                Write-Error "Dependency $($dependency.name):$($dependency.version) - Installed wrong version ($($installedVersion))"
            }
        }
    }
}

# Defining recipe
$recipes = $deploymentManifest.$task
$universalRecipes = $false
if ($recipes -eq $null) {
    $recipes = $deploymentManifest["install-upgrade-uninstall"]
    $universalRecipes = $true
}
if ($recipes -eq $null) {
    throw "Manifest is missing $task declaration."   
}

# Find target environment in the deploymentManifest
$installedEnvironment = Read-EnvEnvironment
$envType = $installedEnvironment["type"]
$envVersion = $installedEnvironment["version"]
if ($envType -eq $null -or $envVersion -eq $null) {
    throw "Environment type or version are missing in the environment config map"
}

# Check for env type
$targetRecipe = $null
foreach ($recipe in $recipes) {
    foreach ($env in $recipe.environments) {
        if ($env.name -eq $envType) {
            $targetRecipe = $recipe
            break
        }
    }
}
if ($targetRecipe -eq $null) {
    throw "There are no steps for environment $envType in the deploymentManifest"
}

# Check for env version
foreach ($recipe in $recipes) {
    $deploymentManifestRecipeEnv = $recipe.environments | Where-Object {$_.name -eq $envType}
    if (!(Test-AppVersion -Version $envVersion -Pattern $deploymentManifestRecipeEnv.version)) {
        throw "Environment version ($envVersion) doesn't match pattern in deploymentManifest ($($deploymentManifestRecipeEnv.version))"
    }
}

Write-Host "`n***** Executing $Task actions for environment $envType... *****"

# Reverse uninstall steps for universal recipe (install-upgrade-uninstall)
$steps = $targetRecipe.steps
if ($task -eq "uninstall" -and $universalRecipes) {
    [array]::Reverse($steps)
}

$stepNumber = 0
$actionType = $null
# Executing custom actions changes the true root path, so we're saving it here
$rootPathBackup = $rootPath
foreach ($step in $steps) {
    $stepNumber++
    $actionPath = $null
    $actionName = $null
    if ($step.action -ne $null) {
        $actionType = "standard"
        $actionName = $step.action
        $actionPath = "$rootPath/../$actionName"
    } elseif ($step["custom-action"] -ne $null) {
        $actionType = "custom"
        $actionName = $step["custom-action"]
        $actionPath = "$packagePath/$actionName"
    } else {
        throw "Step #$stepNumber is missing 'action' or 'custom-action' in the deploymentManifest"
    }

    # Skip on certain tasks
    $executeOn = $step["execute-on"]
    if ($executeOn -ne $null) {
        if (-not ($executeOn -contains $task)) {
            continue;
        }
    }

    # Check if action exists
    if (-not (Test-Path -Path $actionPath)) {
        throw "$actionType action '$actionName' is not found at $actionPath."
    }
    if (-not (Test-Path -Path "$actionPath/$Task.ps1")) {
        throw "$actionType action '$actionName' does not support $Task steps"
    }

    # Define action parameters
    $actionParams = @{
        PackagePath=$packagePath;
        Partition=$Partition;
        TempPath="$rootPath/../temp"
    }

    Write-Host "`n***** Started $actionType action '$actionName' *****`n"

    . "$actionPath/$Task.ps1" -AppManifest $appManifest -DeploymentManifest $deploymentManifest -Config $config -Context $step -Params $actionParams

    Write-Host "`n***** Completed $actionType action '$actionName' *****"

    # Executing custom actions changes the true root path, so here we're restoring it
    $rootPath = $rootPathBackup
}

# Write down version of the installed application
if ($task -eq "install" -or $task -eq "upgrade") {
    $applications[$appManifest.name] = $appManifest.version
} elseif ($task -eq "uninstall") {
    $applications.Remove($appManifest.name)
}
Write-EnvApplications -Applications $applications