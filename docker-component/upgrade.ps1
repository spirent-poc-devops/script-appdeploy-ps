#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [hashtable] $AppManifest,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [hashtable] $DeploymentManifest,

    [Parameter(Mandatory=$true, Position=2)]
    [hashtable] $Config,

    [Parameter(Mandatory=$true, Position=3)]
    [hashtable] $Context,

    [Parameter(Mandatory=$true, Position=4)]
    [hashtable] $Params
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifest -eq $null) {
    $DeploymentManifest = $AppManifest
}

# Reading parameters
$namespace = $Context.namespace
if ($namespace -eq $null) {
    throw "Namespace is missing in the action step"
}

$component = $Context.component
if ($component -eq $null) {
    throw "Component is missing in the action step"
}
$component = Get-AppComponent -Manifest $AppManifest -Component $Context.component
$image = "$($component.registry)/$($component.name):$($component.version)"

$imagePullPolicy = $Context.image_pull_policy
if ($imagePullPolicy -eq $null) {
    $imagePullPolicy = "Always" #IfNotPresent
}

# Set deployment replicas
$deploymentReplicas = ""
if ($Context.replicas -ne $null) {
    $deploymentReplicas = "  replicas: $($Context.replicas)"
} elseif ($Context.hpa -ne $null) {
    $deploymentReplicas = "  replicas: $($Context.hpa.min_replicas)"
} else {
    $deploymentReplicas = "  replicas: 1"
}

# k8s deployment
$name = $Context.name
if ($name -eq $null) {
    throw "Component name is missing in the action step"
}

$envVars = "        env: `n"
$envVarCount = 0
if($Context.env_variables -ne $null){
    foreach ($envVarName in $($Context.env_variables.Keys)) {
        if($envVarCount -gt 0){
            $envVars += "`n"
        }

        if ($envVarName -eq "env_field_refs") {
            # process fieldRef variables
            foreach ($linkedEnvVarName in $($Context.env_variables["env_field_refs"].Keys)) {
                $envVars += "          - name: $linkedEnvVarName `n"
                $envVars += "            valueFrom: $($Context.env_variables["env_field_refs"][$linkedEnvVarName]) "
                $envVarCount++;
            }
        } else {
            # regular env var
            $envVars += "          - name: $envVarName `n"
            $envVars += "            value: `"$($Context.env_variables[$envVarName])`" "
            $envVarCount++;
        }
    }
}

if($Context.env_mappings -ne $null){
    foreach ($envMappings in $Context.env_mappings) {
        $configName = $($envMappings["name"])
        $objType = $($envMappings["object_type"])
        foreach ($envVarName in $($envMappings["mappings"].Keys)) {
            if($envVarCount -gt 0){
                $envVars += "`n"
            }
            if($objType -ne "secret") {
                $objType = "configMap"
            }
            # Note - there are 2 more spaces here than in docker-service!
            $envVars += "          - name: $envVarName `n"
            $envVars += "            valueFrom: `n"
            $envVars += "              $($objType)KeyRef: `n"
            $envVars += "                name: $configName `n"
            $envVars += "                key: $($envMappings["mappings"][$envVarName]) "

            $envVarCount++;
        }
    }
}

# Set custom volumes, if required
if ($Context.volumes -ne $null -and $Context.volumes.enabled -ne $false) {
    $volumeMounts = "        volumeMounts:`n"
    $volumes = "      volumes:`n"

    foreach ($volume in $Context.volumes) {
        $volumeMounts += "        - name: $($volume.name)`n"
        $volumeMounts += "          mountPath: $($volume.mount_path)`n"

        $volumes += "      - name: $($volume.name)`n"
        if ($volume.data.type -eq "emptyDir") {
            $volumes += "        emptyDir: {}`n"
        } elseif ($volume.data.type -eq "configMap") {
            $volumes += "        configMap:`n"
            $volumes += "          name: $($volume.data.name)`n"
        } elseif ($volume.data.type -eq "secret") {
            $volumes += "        secret:`n"
            $volumes += "          secretName: $($volume.data.name)`n"
        }
    }
}

$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
$inputPath = "$rootPath/templates/k8s/component.yml"

# Set deployment resources
$resources = ""
    if ($Context.resources.requests -ne $null) {
        $resources += "          requests:`n"
        $resources += "            cpu:  $($Context.resources.requests.cpu)`n"
        $resources += "            memory: $($Context.resources.requests.memory)`n"
    }
    if ($Context.resources.limits -ne $null) {
        $resources += "          limits:`n"
        $resources += "            memory: $($Context.resources.limits.memory)`n"
    }

# Set k8s deployment api version
$deploymentApiVersion = "apps/v1"
if ($Context.deployment_api_version -ne $null) {
    $deploymentApiVersion = $Context.deployment_api_version
}

    # Set deployment annotations
$deploymentAnnotations = ""
if ($Context.deployment_annotations -ne $null) {
    $deploymentAnnotations = "  annotations:`n"
    foreach ($deploymentAnnotation in $Context.deployment_annotations) {
        $deploymentAnnotations += "    $($deploymentAnnotation.name): `"$($deploymentAnnotation.value)`"`n"
    }
}

# Set additional containers
$additionalContainers = ""
if ($Context.additional_containers -ne $null) {
    
    foreach ($additionalContainer in $Context.additional_containers) {
        $additionalContainers += "      - name: $($additionalContainer.name)`n"
        $additionalContainers += "        image: $($additionalContainer.image)`n"
        
        if ($additionalContainer.resources -ne $null) {
            $additionalContainers += "        resources:`n"
            if ($additionalContainer.resources.requests -ne $null) {
                $additionalContainers += "          requests:`n"
                $additionalContainers += "            cpu: $($additionalContainer.resources.requests.cpu)`n"
                $additionalContainers += "            memory: $($additionalContainer.resources.requests.memory)`n"
            }
            if ($additionalContainer.resources.limits -ne $null) {
                $additionalContainers += "          limits:`n"
                $additionalContainers += "            memory: $($additionalContainer.resources.limits.memory)`n"
            }
        }

        if ($additionalContainer.args -ne $null) {
            $additionalContainers += "        args:`n"
            foreach ($additionalContainerArg in $additionalContainer.args) {
                $additionalContainers += "        - $additionalContainerArg`n"
            }
        }

        if ($additionalContainer.volumes -ne $null) {
            $additionalContainers += "        volumeMounts:`n"
            foreach ($additionalContainerVolume in $additionalContainer.volumes) {
                $additionalContainers += "          - name: $($additionalContainerVolume.name)`n"
                $additionalContainers += "            mountPath: $($additionalContainerVolume.mount_path)`n"
            }
        }
    }
}

# Enable/Disable imagePullSecret
$dockerAuthEnabled = ConvertTo-EnvBoolean $Context.docker_auth
if($dockerAuthEnabled) {
    $imagePullSecrets =  "      imagePullSecrets: `n"
    $imagePullSecrets += "        - name: auth "
}

$tempPath = $Params.TempPath
$outputPath = "$tempPath/component-$name.yml"

$templateParams = @{ 
    api_version = $deploymentApiVersion
    name = $name
    namespace = $namespace
    image = $image
    image_pull_policy = $imagePullPolicy
    deployment_replicas = $deploymentReplicas
    volume_mounts = $volumeMounts
    env_vars = $envVars
    volumes = $volumes
    resources = $resources
    deployment_annotations = $deploymentAnnotations
    additional_containers = $additionalContainers
    image_pull_secrets = $imagePullSecrets
}

Build-EnvTemplate -InputPath $inputPath -OutputPath $outputPath -Params1 $templateParams

# Create hpa if required
if ($Context.hpa -ne $null) {
    $templateParams = @{ 
        name = $name
        namespace = $namespace
        min_replicas = $Context.hpa.min_replicas
        max_replicas = $Context.hpa.max_replicas
        target_cpu = $Context.hpa.target_cpu
    }

    $hpaOutPath = "$tempPath/hpa-$name.yml"
    Build-EnvTemplate -InputPath "$rootPath/templates/k8s/hpa.yml" -OutputPath $hpaOutPath -Params1 $templateParams

    kubectl apply -f $hpaOutPath
    Remove-Item -Path $hpaOutPath
}

# Create vpa if required
if ($Context.vpa -ne $null -and $Context.vpa.enabled) {
    $templateParams = @{ 
        name = $name
        namespace = $namespace
    }

    $vpaOutPath = "$tempPath/vpa-$name.yml"
    Build-EnvTemplate -InputPath "$rootPath/templates/k8s/vpa.yml" -OutputPath $vpaOutPath -Params1 $templateParams

    kubectl apply -f $vpaOutPath
    Remove-Item -Path $vpaOutPath
}

kubectl apply -f $outputPath

Remove-Item -Path $outputPath