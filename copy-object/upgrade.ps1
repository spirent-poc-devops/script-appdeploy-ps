#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [hashtable] $AppManifest,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [hashtable] $DeploymentManifest,

    [Parameter(Mandatory=$true, Position=2)]
    [hashtable] $Config,

    [Parameter(Mandatory=$true, Position=3)]
    [hashtable] $Context,

    [Parameter(Mandatory=$true, Position=4)]
    [hashtable] $Params
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifest -eq $null) {
    $DeploymentManifest = $AppManifest
}

# k8s deployment
# Reading parameters
$destNS = $Context.to_namespace
$objType = $Context.object_type # e.g. configmap || secret
$name = $Context.name
if($Context.from_namespace -ne $null){
    $srcNS = $Context.from_namespace

    # Copy configmap or secret from the source namespace into the destination namespace
    kubectl get $objType $name --namespace=$srcNS -o yaml | % {$_.replace("namespace: $($srcNS)","namespace: $($destNS)")} | kubectl create -f -
}
elseif($Context.file_path -ne $null){
    $filePath = "$($Params.PackagePath)/$($Context.file_path)"

    # Create configmap or secret in the destination namespace using the file at the given path
    if($objType -eq "secret"){
        $objType += " generic"
    } 
    $cmd = "kubectl create $objType $name --namespace=$destNS --from-file=`"$filePath`" --dry-run=client -o yaml | kubectl apply -f -"
    Invoke-Expression $cmd
}
else{
    Write-Host "`"from_namespace`" or `"file_path`" must be included in the step"
}