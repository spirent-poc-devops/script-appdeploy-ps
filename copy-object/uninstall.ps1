#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [hashtable] $AppManifest,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [hashtable] $DeploymentManifest,

    [Parameter(Mandatory=$true, Position=2)]
    [hashtable] $Config,

    [Parameter(Mandatory=$true, Position=3)]
    [hashtable] $Context,

    [Parameter(Mandatory=$true, Position=4)]
    [hashtable] $Params
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifest -eq $null) {
    $DeploymentManifest = $AppManifest
}

# k8s deployment
# Reading parameters
$srcNS = $Context.from_namespace
$destNS = $Context.to_namespace
$objType = $Context.object_type # e.g. configmap || secret
$name = $Context.name

# Delete connection params and credentials that were copied 
# from the infrastructure's connection config map and secret
kubectl delete $objType $name --namespace=$destNS