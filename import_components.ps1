#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $AppManifestPath,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [string] $DeploymentManifestPath,

    [Alias("Config")]
    [Parameter(Mandatory=$true, Position=2)]
    [string] $ConfigPath,

    [Parameter(Mandatory=$false, Position=3)]
    [string] $KubernetesPrefix = "k8s"
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifestPath -eq "") {
    $DeploymentManifestPath = $AppManifestPath
}

# Stop on error
$ErrorActionPreference = "Stop"

# Load common functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/common/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Read manifest and config
$appManifest = Read-AppManifest -ManifestPath $AppManifestPath
$deploymentManifest = Read-AppManifest -ManifestPath $DeploymentManifestPath
$config = Read-EnvConfig -ConfigPath $ConfigPath

# Read config values
$k8s_username = Get-EnvMapValue -Map $config -Key "$KubernetesPrefix.username"
$k8s_master_ip = Get-EnvMapValue -Map $config -Key "$KubernetesPrefix.master_ip"
$k8s_workerips = Get-EnvMapValue -Map $config -Key "$KubernetesPrefix.worker_ips"
$k8s_private_key_path = Get-EnvMapValue -Map $config -Key "$KubernetesPrefix.private_key_path"

# Verify cache directory existence
$packagePath = (Get-Item $AppManifestPath).Directory.FullName
$cachePath = "$packagePath/cache"
if (-not (Test-Path -Path $cachePath)) {
    Write-Error "Missing $packagePath/cache directory. Make sure you already cached images."
}

# Create docker_import folder
if (-not (Test-Path -Path "$rootPath/temp/docker_import")) {
    $null = New-Item -Path "$rootPath/temp/docker_import" -ItemType "directory"
} else {
    # Clear temp/docker_import
    Remove-Item "$rootPath/temp/docker_import/*" -Recurse -Force
}
# Prepare import script which will run on each node by ansible script
$loadScriptContent = "#!/bin/bash`n"
foreach ($component in $appManifest.components) {
    if ($component.type -eq "container") {
        $registry = $component.registry
        # Check for tar file presence
        $artifactPath = "$cachePath/$($component.name)_$($component.version).tar"
        if (Test-Path -Path $artifactPath) {
            Write-Host "Found image at '$artifactPath'"
            Copy-Item -Path $artifactPath -Destination "$rootPath/temp/docker_import/$($component.name)_$($component.version).tar" -Force
            $loadScriptContent += "docker load --input `"/home/$k8s_username/docker_import/$($component.name)_$($component.version).tar`"`n"
        } else {
            Write-Error "Can't load $registry/$($component.name):$($component.version). '$artifactPath' doesn't exists"
        }
    }
}
# Create import script
Set-Content -Path "$rootPath/temp/docker_import/import.sh" -Value $loadScriptContent

# Set k8s ssh key path
$sshPrivateKeyPath = "$($ConfigPath.SubString(0,$ConfigPath.IndexOf("config")))$k8s_private_key_path"

# Prepare hosts file
$index = 0
$ansibleInventory = @("[k8s_nodes]")
$ansibleInventory += "node$($index) ansible_host=$k8s_master_ip ansible_ssh_user=$k8s_username ansible_ssh_private_key_file=$sshPrivateKeyPath"
foreach($k8sNode in $k8s_workerips) {
    $index++
    $ansibleInventory += "node$($index) ansible_host=$k8sNode ansible_ssh_user=$k8s_username ansible_ssh_private_key_file=$sshPrivateKeyPath"
}
Set-Content -Path "$rootPath/temp/k8s_ansible_hosts" -Value $ansibleInventory

# Whitelist nodes
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$rootPath/temp/k8s_ansible_hosts" "$rootPath/templates/ssh_keyscan_playbook.yml"
} else {
    ansible-playbook -i "$rootPath/temp/k8s_ansible_hosts" "$rootPath/templates/ssh_keyscan_playbook.yml"
}

# Import images on each node
$templateParams = @{ k8s_username=$k8s_username }
Build-EnvTemplate -InputPath "$($rootPath)/templates/import_images_playbook.yml" -OutputPath "$($rootPath)/temp/import_images_playbook.yml" -Params1 $templateParams
if ($config["extended_ansible_logs"]) {
    ansible-playbook -v -i "$rootPath/temp/k8s_ansible_hosts" "$rootPath/temp/import_images_playbook.yml"
} else {
    ansible-playbook -i "$rootPath/temp/k8s_ansible_hosts" "$rootPath/temp/import_images_playbook.yml"
}

Write-Host "`n***** Images for $($appManifest.name):$($appManifest.version) imported on k8s nodes *****`n"
