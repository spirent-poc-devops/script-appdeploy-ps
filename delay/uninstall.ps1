#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [hashtable] $AppManifest,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [hashtable] $DeploymentManifest,

    [Parameter(Mandatory=$true, Position=2)]
    [hashtable] $Config,

    [Parameter(Mandatory=$true, Position=3)]
    [hashtable] $Context,

    [Parameter(Mandatory=$true, Position=4)]
    [hashtable] $Params
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifest -eq $null) {
    $DeploymentManifest = $AppManifest
}

# Delay for a few seconds
$seconds = $Context.seconds
if ($seconds -ne $null) {
    $seconds = 1
    Write-Host "Waiting $seconds seconds..."
    Start-Sleep -Seconds $seconds
}

# Delay until URL responds
$testUrl = $Context.test_url
if ($testUrl -ne $null) {
    Write-Host "Trying to connect to $testUrl"
    while ($true) {
        try {
            Invoke-WebRequest -UseBasicParsing $testUrl | Out-Null
            break
        } catch {
            Start-Sleep -Seconds 5
            Write-Host "Failed to connect. Retrying..."
        }
    }
}
