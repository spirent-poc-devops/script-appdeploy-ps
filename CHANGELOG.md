# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> Spirent VisionWorks Application Deployment Scripts

### 1.0.0 (2021-02-11)

Initial release of the application deployment scripts project

#### Features
* Install, upgrade, uninstall and cache scripts
* Common functions
* Installation of prerequisites
* Functions to process semantic versions

#### Breaking Changes
No breaking changes since this is the first version

#### Bug Fixes
No fixes in this version

