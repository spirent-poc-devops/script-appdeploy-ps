#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $AppManifestPath,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [string] $DeploymentManifestPath,

    [Parameter(Mandatory=$false, Position=4)]
    [string] $Version,

    [Parameter(Mandatory=$false, Position=5)]
    [string] $Namespace
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifestPath -eq "") {
    $DeploymentManifestPath = $AppManifestPath
}

# Stop on error
$ErrorActionPreference = "Stop"

# Load common functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/common/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Read connection parameters
$config = Read-EnvParams

# Check for and fill in templated vars in appManifest, then read the appManifest
$appManifestTemplatePath = $AppManifestPath
$AppManifestPath = "$rootPath/temp/app_manifest.json"
Build-EnvTemplate -InputPath $appManifestTemplatePath -OutputPath $AppManifestPath -Params1 $config
$appManifest = Read-AppManifest -ManifestPath $AppManifestPath

# Set default namespace
if ($Namespace -eq $null -or $Namespace -eq "") {
    $Namespace = $appManifest.name
}

# Check for and fill in templated vars in deploymentManifest, then read the deploymentManifest
$deploymentManifestTemplatePath = $DeploymentManifestPath
$DeploymentManifestPath = "$rootPath/temp/deployment_manifest.json"
Build-EnvTemplate -InputPath $deploymentManifestTemplatePath -OutputPath $DeploymentManifestPath -Params1 $config
$deploymentManifest = Read-AppManifest -ManifestPath $DeploymentManifestPath

# Update appManifest version
if ($Version -ne $null -and $Version -ne "" -and $Version -ne $appManifest.version) {
    $sourceAppManifest = Get-Content -Path $appManifestTemplatePath | Out-String | ConvertFrom-Json
    $sourceAppManifest.version = $Version

    $sourceAppManifest | ConvertTo-Json -Depth 20 | Set-Content -Path $appManifestTemplatePath
}

# K8S env
$k8sComponents = kubectl get pods -n $namespace -o=jsonpath='{range .items[*]}{\"\n\"}{range .spec.containers[0]}{.image}{end}'
# Remove duplicates (if replicas exist)
$k8sComponents = $k8sComponents | Select-Object -Unique
# Trim first empty item
if ($k8sComponents[0] -eq "") {
    $k8sComponents = $k8sComponents[1..($k8sComponents.Length-1)]
}

# Iterate through application component
$components = $appManifest.components | Where-Object {$_.type -eq "container"}

foreach ($component in $components) {
    Write-Host "Processing component $($component.name)..."
    $image = "$($component.registry)/$($component.name)"
    $k8sComponentFullImage = $k8sComponents | Where-Object {$_ -match $image}
    if ($k8sComponentFullImage -eq $null) {
        # If env doesn't have component from appManifest throw an error
        throw "Component $($component.name) is missing in the environment."
    }
    $k8sComponentVersion = $k8sComponentFullImage.Split(":")[1]

    if ($component.version -ne $k8sComponentVersion) {
        Write-Host "Component $($component.name) uses image $k8sComponentFullImage, but version in appManifest is $($component.version)"
        Write-Host "Updating component version in the appManifest file..."
        $newComponent = $appManifest.components | Where-Object {$_.name -eq $component.name}
        $newComponent.version = $k8sComponentVersion

        $sourceAppManifest = Get-Content -Path $appManifestTemplatePath | Out-String | ConvertFrom-Json
        $newComponent = $sourceAppManifest.components | Where-Object {$_.name -eq $component.name}
        $newComponent.version = $k8sComponentVersion

        $sourceAppManifest | ConvertTo-Json -Depth 20 | Set-Content -Path $appManifestTemplatePath
    }
}

# Verify that all components running in env present 
foreach ($k8sComponent in $k8sComponents) {
    $image = $k8sComponent.Split(":")[0]
    if (($components | Where-Object {"$($_.registry)/$($_.name)" -eq $image}) -eq $null) {
        Write-Host "Component with image $k8sComponent is running in environment, but missing in appManifest."
    }
}
