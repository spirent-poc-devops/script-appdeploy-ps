#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [hashtable] $AppManifest,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [hashtable] $DeploymentManifest,

    [Parameter(Mandatory=$true, Position=2)]
    [hashtable] $Config,

    [Parameter(Mandatory=$true, Position=3)]
    [hashtable] $Context,

    [Parameter(Mandatory=$true, Position=4)]
    [hashtable] $Params
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifest -eq $null) {
    $DeploymentManifest = $AppManifest
}

# Reading parameters
$namespace = $Context.namespace
if ($namespace -eq $null) {
    throw "Namespace is missing in the action step"
}

# k8s deployment
$routes = ""
$rewriteTarget = ""
if($Context.routes -ne $null) {
    if($Context.rewrite -ne $null -and $Context.rewrite -ne ""){
        $rewriteTarget = "  annotations:"
        $rewriteTarget += "    nginx.ingress.kubernetes.io/rewrite-target: " + $Context.rewrite
    }

    foreach ($route in $($Context.routes)) {
        $routes += "  - http: `n"
        $routes += "      paths: `n"
        if ($route.prefix -ne $null) {
            $routes += "      - path: $($route.prefix) `n"
            $routes += "        pathType: Prefix `n"
        } else {
            $routes += "      - path: $($route.path) `n"
            $routes += "        pathType: Exact `n"
        }
        $routes += "        backend: `n"
        $routes += "          service: `n"
        $routes += "            name: $($route.service) `n"
        $routes += "            port: `n"
        $routes += "              number: $($route.port) `n"
    }
}

$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
$inputPath = "$rootPath/templates/ingress.yml"

$tempPath = $Params.TempPath
$outputPath = "$tempPath/ingress.yml"

$templateParams = @{ 
    namespace = $namespace
    rewrite_target = $rewriteTarget
    routes = $routes
}

Build-EnvTemplate -InputPath $inputPath -OutputPath $outputPath -Params1 $templateParams

kubectl create -f $outputPath

Remove-Item -Path $outputPath