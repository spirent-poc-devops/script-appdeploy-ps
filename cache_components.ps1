#!/usr/bin/env pwsh

param
(
    [Alias("Manifest", "Application")]
    [Parameter(Mandatory=$true, Position=0)]
    [string] $AppManifestPath,

    [Alias("Deployment")]
    [Parameter(Mandatory=$false, Position=1)]
    [string] $DeploymentManifestPath,

    [Alias("Config")]
    [Parameter(Mandatory=$true, Position=2)]
    [string] $ConfigPath
)

# Use single manifest if Deployment manifest not set
if ($DeploymentManifestPath -eq "") {
    $DeploymentManifestPath = $AppManifestPath
}

# Stop on error
$ErrorActionPreference = "Stop"

# Load common functions
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }
. "$($rootPath)/common/include.ps1"
$rootPath = $PSScriptRoot
if ($rootPath -eq "") { $rootPath = "." }

# Read manifest and config
$appManifest = Read-AppManifest -ManifestPath $AppManifestPath
$deploymentManifest = Read-AppManifest -ManifestPath $DeploymentManifestPath
$config = Read-EnvConfig -ConfigPath $ConfigPath

Write-Host "***** Caching references for $($appManifest.name):$($appManifest.version) *****"

# Define cache directory and create if it doesn't exist
$packagePath = (Get-Item $AppManifestPath).Directory.FullName
$cachePath = "$packagePath/cache"
if (-not (Test-Path -Path $cachePath)) {
    $null = New-Item -Path $cachePath -ItemType "directory"
}

# Login to docker registry
sudo docker login (Get-EnvMapValue -Map $config -Key "docker_registry")  -u (Get-EnvMapValue -Map $config -Key "docker_username") -p (Get-EnvMapValue -Map $config -Key "docker_password")

foreach ($component in $appManifest.components) {
    Write-Host "`n***** Caching component $($component.name):$($component.version) *****`n"

    # Define artifact type and reference
    $artifactType = $null
    $artifactRef = $null
    if ($component.type -eq "container") {
        $artifactType = "docker"
        $registry = $component.registry
        $artifactRef = "$registry/$($component.name):$($component.version)"
    } elseif (($component.type -eq "process") -or ($component.type -eq "lambda")) {
        $artifactType = "archive"
        $artifactRef = $component.link
        if ($artifactRef -eq $null) {
            #  Todo: $artifactRef = "http://url.com/releases/$($appManifest.name)/$($component.name)-$($component.version).zip"
        }
    }

    # Cache artifact depending on its type
    switch ($artifactType) {
        "docker" {
            Write-Host "Pulling docker image $artifactRef ..."
            sudo docker pull $artifactRef
            sudo docker save --output "$cachePath/$($component.name)_$($component.version).tar" $artifactRef
            sudo chown ubuntu:ubuntu "$cachePath/$($component.name)_$($component.version).tar"
        }
        "archive" {
            Write-Host "Downloading archive $artifactRef ..."
            # Todo... Complete implementation
        }
        default {
            Write-Warning "Component type is unknown. Skipping..."
        }
    }
    Write-Host "***** Completed caching component $($component.name):$($component.version) *****"
}

Write-Host "***** Completed caching references for $($appManifest.name):$($appManifest.version) *****"